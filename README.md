# Exemple of SQL statements to send to a local db

## Dependencies

- psql
- docker
- terraform

## Create local database

```
export PGPASSWORD=pwd
docker run --name some-postgres -e POSTGRES_PASSWORD=$PGPASSWORD -d -p 5432:5432 postgres
```

## Create table

```
export PGPASSWORD=pwd; psql -h localhost -p 5432 -U postgres -f test.sql
```

## User terraform config

```
terraform init
terraform plan
terraform apply # write yes on prompt
```
