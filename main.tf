locals {
  user_data = templatefile("sql.tpl", { tables = ["tmp", "tmp2"] })
  pass = "pwd"
}

resource "null_resource" "local_db" {
  provisioner "local-exec" {
    command = "export PGPASSWORD='${local.pass}';psql --host=localhost --port=5432 --username=postgres --dbname=postgres -c \"${local.user_data}\""
  }
}

/* output "hello" { */
/*   value = local.user_data */
/* } */
